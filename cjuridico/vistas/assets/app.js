var app = angular.module('cjuridico', [
    'ui.router',
    'LocalStorageModule',
    'ngTable'
]);
app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/Inicio");

        $stateProvider.
        state('app', {
            url: "/app",
            templateUrl: "navbar.html"
        }).state('inicio', {
            url: "/Inicio",
            templateUrl: "home.html"
        }).state('login', {
            url: "/Login",
            templateUrl: "Login.html"
        }).state('asesores', {
            url: "/Asesores",
            templateUrl: "asesores.html"
        }).state('convenios', {
            url: "/Convenios",
            templateUrl: "convenios.html"
        }).state('ubicacion', {
            url: "/Ubicacion",
            templateUrl: "ubicacion.html"
        }).state('horario', {
            url: "/Horarios",
            templateUrl: "horario.html"
        }).state('reglamento', {
            url: "/Reglamento",
            templateUrl: "reglamento.html"
        }).state('preguntas', {
            url: "/PreguntasFrecuentes",
            templateUrl: "preguntas.html"
        }).state('registro', {
            url: "/Registro",
            templateUrl: "Registro.html"
        }).state('historialConsultas', {
            url: "/HistorialConsultas",
            templateUrl: "Consultas.html"
        }).state('AddConsultas', {
            url: "/NuevaConsulta",
            templateUrl: "Consultas.html"
        }).state('perfil', {
            url: "/MiCuenta",
            templateUrl: "Perfil.html"
        });
    }
]);