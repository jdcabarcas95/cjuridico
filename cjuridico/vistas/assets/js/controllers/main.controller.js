(function() {
    'use strict';
    app.controller('mainController', mainController);
    //Se Inyectan las diirectivas a utilizar dentro del controlador
    mainController.$inject = ['mainService', 'localStorageService', '$location','JuridicoService'];
    /*En este controlador se controla la pagina inicial algunos datos y/o Animaciones*/
    function mainController(mainService, localStorageService, $location,JuridicoService) {
        var control = this;
        control.notificaciones = [];
        control.datosPersonales = {};
        control.meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
            'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ];

        control.acercaDe = function() {
            $("#resena-logo").hide();
            $("#description-logo").show();
            $("#mision-logo").hide();
            $("#vision-logo").hide();
            $("#objetivos-logo").hide();
        };
        control.resenaHist = function() {
            $("#resena-logo").show();
            $("#description-logo").hide();
            $("#mision-logo").hide();
            $("#vision-logo").hide();
            $("#objetivos-logo").hide();
        };
        control.mision = function() {
            $("#resena-logo").hide();
            $("#description-logo").hide();
            $("#mision-logo").show();
            $("#vision-logo").hide();
            $("#objetivos-logo").hide();
        };
        control.vision = function() {
            $("#resena-logo").hide();
            $("#description-logo").hide();
            $("#mision-logo").hide();
            $("#vision-logo").show();
            $("#objetivos-logo").hide();
        };
        control.objetivos = function() {
            $("#resena-logo").hide();
            $("#description-logo").hide();
            $("#mision-logo").hide();
            $("#vision-logo").hide();
            $("#objetivos-logo").show();
        };
        //Carga Los estados civiles estimulados en la base de datos
        control.cargarEstadoCivil = function() {
            JuridicoService.estado_civil().then(function(response) {
                control.estados = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        control.cargarSexo = function() {
            JuridicoService.cargar_sexo().then(function(response) {
                control.sexos = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        control.cargarDepartamentos = function() {
            JuridicoService.cargar_departamentos().then(function(response) {
                control.departamentos = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        control.cargarMunicipios = function(id) {
            JuridicoService.cargar_municipios(id).then(function(response) {
                control.municipios = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        control.verificarConsulta = function() {
            mainService.cargarConsultas(control.datos.radicadoConsultar).then(function(response) {
                control.resultado = response.data;
                if (!control.resultado[0].area_asignada || control.resultado[0].area_asignada == "") {
                    control.resultado[0].estado = "SIN ASIGNAR";
                } else {
                    control.resultado[0].estado = "ASIGNADA";
                }
                var fecha = new Date(control.resultado[0].fecha_consulta);
                control.resultado[0].fecha = String(fecha.getDate() + " de " + control.meses[fecha.getMonth()] + " de " + fecha.getFullYear());
                $('#myModal').modal('show');
            });
        };
        control.informacionPersonal = function() {
            mainService.cargarInformacionPersonal(control.usuario.id_persona).then(function(response) {
                control.datosPersonales = response.data[0];
                console.log(control.datosPersonales);
            });
        };
        /*Funcion Para cerrar la Sesion del usuario si la hay*/
        control.logOut = function() {
            localStorageService.remove('user');
            var actual = window.location.search;
            window.location.href = actual;
        };
        /*Verifica si la sesion la fue creada*/
        control.verificarSesion = function() {
            if (localStorageService.get('user') == null || localStorageService.get('user') == "") {
                control.sesion = false;
            } else {
                control.usuario = (localStorageService.get('user'));
                control.sesion = true;
            }
        };

        control.verificarSesion();
    }
})();